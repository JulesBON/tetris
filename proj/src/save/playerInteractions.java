package save;

import model.player.BestPlayers;
import model.player.Player;

import java.io.*;
import java.util.ArrayList;

public class playerInteractions {
    public static final String SAVE_PATH = "saves/save.txt";
    private BestPlayers bestPlayers = new BestPlayers();
    private ArrayList<Player> bestPlayersArray = bestPlayers.getBestPlayers();


    public void save(Player currPlayer)
    {
        for (int i = 0 ; i < bestPlayersArray.size()-1 ; i++) {
            if(currPlayer.getScore() >= bestPlayersArray.get(i).getScore() && currPlayer.getScore() <= bestPlayersArray.get(i+1).getScore()){
                bestPlayersArray.remove(i);
                bestPlayersArray.add(i, currPlayer);
            }
        }

        if(currPlayer.getScore() >= bestPlayersArray.get(bestPlayersArray.size()-1).getScore()){
            bestPlayersArray.remove(bestPlayersArray.size()-1);
            bestPlayersArray.add(bestPlayersArray.size()-1, currPlayer);
        }

        bestPlayersArray = bestPlayers.sortBestPlayers(bestPlayersArray);

        File f = new File(SAVE_PATH);
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(f));
            oos.writeObject(bestPlayersArray);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Player> getBestPlayersArray() {
        return bestPlayersArray;
    }

    public void load()
    {
        File f = new File(SAVE_PATH);
        if(f.length() == 0)
            return;
        ObjectInputStream ois;
        try {
            ois = new ObjectInputStream(new FileInputStream(f));
            bestPlayersArray = (ArrayList<Player>) ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
