package model.shape;

import model.game.Game;

public class BlockGenerator {

    public Block generateBlock() throws Exception {
        double block = Math.random();

        if (block <= (double)1/Game.NUMBER_OF_SHAPES)
            return new LTetrimino();
        if (block <= (double)2/Game.NUMBER_OF_SHAPES)
            return new STetrimino();
        if (block <= (double)3/Game.NUMBER_OF_SHAPES)
            return new TTetrimino();
        if(block <= (double)4/Game.NUMBER_OF_SHAPES)
            return new ZTetrimino();
        if(block <= (double)5/Game.NUMBER_OF_SHAPES)
            return new ITetrimino();
        if(block<= (double)6/Game.NUMBER_OF_SHAPES)
            return new SquareTetrimino();
        if(block <= (double)7/Game.NUMBER_OF_SHAPES)
            return new JTetrimino();


        throw new Exception("Block not found");
    }
}
