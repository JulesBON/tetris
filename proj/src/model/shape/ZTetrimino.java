package model.shape;

import javafx.scene.paint.Color;
import model.game.Playground;

public class ZTetrimino extends Block {

    ZTetrimino(){
        tile = new Tile();
        Color shapeColor = Color.web("#FF0000");
        tile.setColor(shapeColor);


        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0) - 1,0, tile.FIRST_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0), 0, tile.SECOND_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0),1, tile.THIRD_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0) + 1, 1, tile.FOURTH_RECTANGLE);

        tile.setPivot(tile.getRectangle(tile.SECOND_RECTANGLE));
    }



    private void rotate(double angle){
        switch ((int) angle)
        {
            case 0:
                tile.setRectanglePosition(tile.getPivotX() + 1, tile.getPivotY()+1,tile.FIRST_RECTANGLE);
                tile.setRectanglePosition(tile.getPivotX() - 1 , tile.getPivotY(),tile.FOURTH_RECTANGLE);
                checkRotation();
                break;
            case RIGHT_ANGLE:
            case -RIGHT_ANGLE:
                tile.setRectanglePosition(tile.getPivotX() + 1 , tile.getPivotY(),tile.FIRST_RECTANGLE);
                tile.setRectanglePosition(tile.getPivotX() + 1 , tile.getPivotY() - 1,tile.FOURTH_RECTANGLE);
                checkRotation();
                break;
        }
    }

    public void clockwiseRotation() {
        if(!MOVER.isShapeMovable(tile) || MOVER.isShapeOnExtremity(tile)) {
            return;
        }
        rotationAngle = (rotationAngle + 90) % 180;

        rotate(rotationAngle);
    }

    public void antiClockwiseRotation() {
        if(!MOVER.isShapeMovable(tile) || MOVER.isShapeOnExtremity(tile)) {
            return;
        }
        rotationAngle = (rotationAngle - 90) % 180;

        rotate(rotationAngle);
    }
}
