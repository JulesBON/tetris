package model.shape;

import model.game.Playground;
import model.movement.ShapeMover;

public abstract class Block{
    double rotationAngle = 0.0;
    Tile tile;

    final ShapeMover MOVER = new ShapeMover();
    static final int RIGHT_ANGLE = 90;
    static final int STRAIGHT_ANGLE = 180;
    static final int REFLEX_ANGLE = 270;

    public Tile getTile() {
        return tile;
    }

    void checkRotation()
    {
        for (int i = 0; i < tile.getNB_OF_RECTANGLES() ; i++){
            if(tile.getRectangleX(i) >= Playground.PLAYGROUND_COLS)
                MOVER.moveLeft(tile);
            if(tile.getRectangleX(i) < 0)
                MOVER.moveRight(tile);
            if(tile.getRectangleY(i) >= Playground.PLAYGROUND_ROWS)
                MOVER.moveTop();
        }
    }

    public abstract void clockwiseRotation();

    public abstract void antiClockwiseRotation();
}
