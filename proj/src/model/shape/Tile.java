package model.shape;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import model.game.Playground;

import java.util.ArrayList;

public class Tile {
    private ArrayList<Rectangle> rectangles;
    private final int NB_OF_RECTANGLES = 4;
    private Rectangle pivot;
    private Color strokeColor = Color.web("#000000");
    private Color shapeColor;

    public final int FIRST_RECTANGLE = 0;
    public final int SECOND_RECTANGLE = 1;
    public final int THIRD_RECTANGLE = 2;
    public final int FOURTH_RECTANGLE = 3;

    public Tile() {
        rectangles = new ArrayList<>();
        for(int i = 0; i < NB_OF_RECTANGLES; i++){
            Rectangle r = new Rectangle(Playground.TILE_SIZE, Playground.TILE_SIZE);
            r.setStroke(strokeColor);
            rectangles.add(i,r);
        }
    }

    public int getNB_OF_RECTANGLES() { return NB_OF_RECTANGLES; }

    public Color getShapeColor() {
        return shapeColor;
    }

    void setPivot(Rectangle pivot) {
        this.pivot = pivot;
    }

    public Rectangle getRectangle(int index){ return rectangles.get(index); }

    void setColor(Color newColor) {
        shapeColor = newColor;
        for(int i = 0; i < NB_OF_RECTANGLES; i++){
            rectangles.get(i).setFill(newColor);
        }
    }

    public void setRectangleColor(Color newColor, int index){ rectangles.get(index).setFill(newColor);}

    public void setRectanglePosition(double x, double y, int index){
        rectangles.get(index).setX(x * Playground.TILE_SIZE);
        rectangles.get(index).setY(y * Playground.TILE_SIZE);
    }

    public int getRectangleX(int index) { return (int) (rectangles.get(index).getX() / Playground.TILE_SIZE);
    }

    public int getRectangleY(int index) {
        return (int) (rectangles.get(index).getY() / Playground.TILE_SIZE);
    }

    public int getPivotX() {
        return (int) pivot.getX() / Playground.TILE_SIZE;
    }

    int getPivotY() {
        return (int) pivot.getY() / Playground.TILE_SIZE;
    }

    public void setRectangleY(double y, int index){
        if(y >= Playground.PLAYGROUND_ROWS)
            return;
        rectangles.get(index).setY(y * Playground.TILE_SIZE);
    }

    public void setRectangleAt(int x, int y, Color paint) {
        for(int i = 0; i < NB_OF_RECTANGLES; i++) {
            if(getRectangleX(i) == x && getRectangleY(i) == y)
                setRectangleColor(paint, i);
        }

    }


    public Paint getRectangleColor(int index) { return rectangles.get(index).getFill(); }
}
