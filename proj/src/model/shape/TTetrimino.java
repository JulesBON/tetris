package model.shape;

import javafx.scene.paint.Color;
import model.game.Playground;

public class TTetrimino extends Block{

    TTetrimino(){
        tile = new Tile();
        Color shapeColor = Color.web("#AA00FF");
        tile.setColor(shapeColor);

        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0) - 1,0, tile.FIRST_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0), 0, tile.SECOND_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0) +1, 0, tile.THIRD_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0),1, tile.FOURTH_RECTANGLE);

        tile.setPivot(tile.getRectangle(tile.SECOND_RECTANGLE));
    }

    private void rotate(double angle){

        switch ((int) angle){
            case 0:
                tile.setRectanglePosition(tile.getPivotX() - 1 , tile.getPivotY(),tile.FIRST_RECTANGLE);
                tile.setRectanglePosition(tile.getPivotX() + 1, tile.getPivotY(),tile.THIRD_RECTANGLE);
                tile.setRectanglePosition(tile.getPivotX() , tile.getPivotY() + 1,tile.FOURTH_RECTANGLE);
                checkRotation();
                break;
            case RIGHT_ANGLE:
                tile.setRectanglePosition(tile.getPivotX(), tile.getPivotY() - 1,tile.THIRD_RECTANGLE);
                checkRotation();
                break;
            case -RIGHT_ANGLE:
                tile.setRectanglePosition(tile.getPivotX(), tile.getPivotY() - 1,tile.FIRST_RECTANGLE);
                checkRotation();
                break;
            case STRAIGHT_ANGLE:
                tile.setRectanglePosition(tile.getPivotX()+1, tile.getPivotY(),tile.FOURTH_RECTANGLE);
                checkRotation();
                break;
            case -STRAIGHT_ANGLE:
                tile.setRectanglePosition(tile.getPivotX()-1, tile.getPivotY(),tile.FOURTH_RECTANGLE);
                checkRotation();
                break;
            case REFLEX_ANGLE:
                tile.setRectanglePosition(tile.getPivotX(), tile.getPivotY() + 1,tile.FIRST_RECTANGLE);
                checkRotation();
                break;
            case -REFLEX_ANGLE:
                tile.setRectanglePosition(tile.getPivotX(), tile.getPivotY()+1,tile.THIRD_RECTANGLE);
                checkRotation();
                break;
        }
    }

    public void clockwiseRotation() {
        if(!MOVER.isShapeMovable(tile) || MOVER.isShapeOnExtremity(tile)) {
            return;
        }
        rotationAngle = (rotationAngle + 90) % 360;

        rotate(rotationAngle);
    }

    public void antiClockwiseRotation() {
        if(!MOVER.isShapeMovable(tile) || MOVER.isShapeOnExtremity(tile)) {
            return;
        }
        rotationAngle = (rotationAngle - 90) % 360;

        rotate(rotationAngle);
    }
}
