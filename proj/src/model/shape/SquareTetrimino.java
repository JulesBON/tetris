package model.shape;

import javafx.scene.paint.Color;
import model.game.Playground;

public class SquareTetrimino extends Block {
    public SquareTetrimino(){
        tile = new Tile();
        Color shapeColor = Color.web("#FFFF00");
        tile.setColor(shapeColor);

        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0),0,tile.FIRST_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0), 1,tile.SECOND_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0) - 1, 0,tile.THIRD_RECTANGLE);
        tile.setRectanglePosition((Playground.PLAYGROUND_COLS / 2.0) - 1,1,tile.FOURTH_RECTANGLE);

        tile.setPivot(tile.getRectangle(tile.SECOND_RECTANGLE));
    }


    @Override
    public void clockwiseRotation() {
        return;
    }

    @Override
    public void antiClockwiseRotation() {
        return;
    }
}
