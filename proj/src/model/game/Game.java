package model.game;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import model.shape.BlockGenerator;

public class Game {
    private  IntegerProperty score = new SimpleIntegerProperty();
    private static Playground gamePlayground  = new Playground();
    private BlockGenerator blockGenerator = new BlockGenerator();
    public static final int NUMBER_OF_SHAPES = 7;
    private boolean isGamePlayable;
    public static final int POINTS_PER_LINES = 40;

    public Game() {
        score.setValue(0);
        isGamePlayable = true;
    }

    public BlockGenerator getBlockGenerator() {
        return blockGenerator;
    }

    public Game getGame(){
        return this;
    }

    public Playground getGamePlayground(){return gamePlayground;}

    public int getScore() { return score.get(); }

    public IntegerProperty scoreProperty() {
        return score;
    }

    public void setScore(int score) {
        this.score.set(score);
    }

    public void setIsGamePlayable(boolean state){
        isGamePlayable = state;
    }

    public boolean getIsGamePlayable(){return isGamePlayable;}
}
