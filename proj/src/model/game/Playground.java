package model.game;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Playground {
    public static final int TILE_SIZE = 35;
    public static final int PLAYGROUND_ROWS = 20;
    public static final int PLAYGROUND_COLS = 10;
    public final static Color BACKGROUND_COLOR = Color.WHITE;
    private static int[][] PLAYGROUND_DIMENSION = new int[PLAYGROUND_ROWS][PLAYGROUND_COLS];
    private static Game currentGame = new Game();
    private static Group playground;

    public Playground() {

        playground = new Group();
        for (int i = 0; i < PLAYGROUND_ROWS; i++) {
            for (int j = 0; j < PLAYGROUND_COLS; j++) {
                Rectangle r = new Rectangle(j * (TILE_SIZE), i * (TILE_SIZE), TILE_SIZE, TILE_SIZE);
                r.setFill(BACKGROUND_COLOR);
                r.setStroke(Color.BLACK);
                playground.getChildren().add(r);
                PLAYGROUND_DIMENSION[i][j] = 0;
                }
        }
    }

    public static Game getCurrentGame() {
        return currentGame;
    }

    public Group getPlayground() {
        return playground;
    }

    public static void setPlaygroundDimension(int i, int j, int val) {
        PLAYGROUND_DIMENSION[i][j] = val;
    }

    public static void setPlaygroundDimension(int line, int val) {
        for (int i = 0; i < PLAYGROUND_COLS; i++) {
            setPlaygroundDimension(line, i, val);
        }
    }

    public static int getPlaygroundDimension(int i, int j) {
        return PLAYGROUND_DIMENSION[i][j];
    }
}