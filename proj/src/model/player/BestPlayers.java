package model.player;

import save.playerInteractions;

import java.io.File;
import java.util.ArrayList;

public class BestPlayers{

    private ArrayList<Player> bestPlayers;

    public BestPlayers() {
        File file = new File(playerInteractions.SAVE_PATH);
        if(file.length() == 0){
            bestPlayers = new ArrayList<>();
            initializeBestPlayers();
        }
    }

    private void initializeBestPlayers(){
        bestPlayers.add(new Player(520,"Jules"));
        bestPlayers.add(new Player(10,"Bob"));
        bestPlayers.add(new Player(348,"Valentin"));
        bestPlayers.add(new Player(264,"Alice"));
        bestPlayers.add(new Player(123,"Arouf"));
        sortBestPlayers(bestPlayers);
    }

    public ArrayList<Player> sortBestPlayers(ArrayList<Player> unsortedArray) {
        unsortedArray.sort((o1, o2) -> o2.getScore() - o1.getScore());
        return unsortedArray;
    }

    public ArrayList<Player> getBestPlayers() {
        return bestPlayers;
    }

}
