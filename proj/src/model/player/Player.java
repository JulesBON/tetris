package model.player;

import java.io.Serializable;

public class Player implements Serializable {
    private static final long serialVersionUID = 6610770750180325418L;
    private int score;
    private String name;

    public Player(int score, String name){
        this.score = score;
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }
}
