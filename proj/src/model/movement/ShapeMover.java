package model.movement;

import model.game.Game;
import model.game.Playground;
import model.shape.Tile;

import java.util.ArrayList;

public class ShapeMover {
    private boolean lockedBlock = false;
    private LineDestroyer lineDestroyer = new LineDestroyer();
    private Game game = Playground.getCurrentGame();
    private Tile tile;
    private ArrayList<Tile> unmovableShapes = new ArrayList<>();




    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public void moveRight(Tile tile) {
        for (int i = 0; i < tile.getNB_OF_RECTANGLES(); i++){
            if(tile.getRectangleX(i) >= Playground.PLAYGROUND_COLS - 1 || !isShapeMovable(tile))
                return;
        }
        for (int i = 0; i < tile.getNB_OF_RECTANGLES(); i++)
            tile.setRectanglePosition(tile.getRectangleX(i) + 1, tile.getRectangleY(i), i);
    }

    public void moveLeft(Tile tile) {
        for (int i = 0; i < tile.getNB_OF_RECTANGLES(); i++){
            if(tile.getRectangleX(i) <= 0 || !isShapeMovable(tile))
                return;
        }
        for (int i = 0; i < tile.getNB_OF_RECTANGLES(); i++)
            tile.setRectanglePosition(tile.getRectangleX(i) - 1, tile.getRectangleY(i), i);
    }

    public void moveBottom(Tile tile) {
        if(isShapeOnFirstLine(tile))
            checkLastLine(unmovableShapes);
        if(islockedBlock() || !isShapeMovable(tile))
            return;

        for (int i = 0; i < tile.getNB_OF_RECTANGLES(); i++)
            tile.setRectanglePosition(tile.getRectangleX(i), tile.getRectangleY(i)+1, i);
    }

    public void moveTop() {
        for (int i = 0; i < tile.getNB_OF_RECTANGLES(); i++)
            tile.setRectanglePosition(tile.getRectangleX(i), tile.getRectangleY(i)-1, i);
    }

    private boolean islockedBlock() {
        return lockedBlock;
    }

    public void setLockedBlock(boolean lockedBlock) {
        this.lockedBlock = lockedBlock;
    }

    public void lockPiece(Tile t) {
        for (int i = 0; i < tile.getNB_OF_RECTANGLES(); i++){
            Playground.setPlaygroundDimension( t.getRectangleY(i), t.getRectangleX(i), 1);
            t.setRectanglePosition(t.getRectangleX(i), t.getRectangleY(i), i);
        }
        setLockedBlock(true);

        game.setScore((int)(game.getScore() +  (Math.random() * tile.getNB_OF_RECTANGLES() +1)));
        unmovableShapes.add(t);
    lineDestroyer.eraseLine(unmovableShapes);
    }

    private boolean isShapeOnFirstLine(Tile t){
        return (t.getRectangleY(t.FIRST_RECTANGLE) == 0
                || t.getRectangleY(t.SECOND_RECTANGLE) == 0
                || t.getRectangleY(t.THIRD_RECTANGLE) == 0
                || t.getRectangleY(t.FOURTH_RECTANGLE) == 0);
    }

    private boolean isShapeOnLastLine(Tile t){
        return (t.getRectangleY(t.FIRST_RECTANGLE) == Playground.PLAYGROUND_ROWS - 1
                || t.getRectangleY(t.SECOND_RECTANGLE) == Playground.PLAYGROUND_ROWS - 1
                || t.getRectangleY(t.THIRD_RECTANGLE) == Playground.PLAYGROUND_ROWS - 1
                || t.getRectangleY(t.FOURTH_RECTANGLE) == Playground.PLAYGROUND_ROWS - 1);
    }

    private void checkLastLine(ArrayList<Tile> unmovableShapes){

        for(Tile t :unmovableShapes) {
            if(!isShapeMovable(t)){
                for (int j = 0; j < t.getNB_OF_RECTANGLES(); j++) {
                    if (t.getRectangleY(j) == 0){
                        game.setIsGamePlayable(false);
                        setLockedBlock(true);
                    }
                }
            }
        }
    }

    public boolean isShapeMovable(Tile tile) {
        return  (
                !isShapeOnLastLine(tile)
                //shape under ?
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.FIRST_RECTANGLE) + 1,  tile.getRectangleX(tile.FIRST_RECTANGLE)) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.SECOND_RECTANGLE) + 1,  tile.getRectangleX(tile.SECOND_RECTANGLE)) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.THIRD_RECTANGLE) + 1, tile.getRectangleX(tile.THIRD_RECTANGLE)) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.FOURTH_RECTANGLE) + 1,  tile.getRectangleX(tile.FOURTH_RECTANGLE)) != 1
        );
    }

    public boolean isShapeOnExtremity(Tile tile) {
        return (tile.getPivotX() == Playground.PLAYGROUND_COLS - 1 || tile.getPivotX() == 0);
    }
    public boolean isShapeMovableLeft (Tile tile){
        return  (
                Playground.getPlaygroundDimension( tile.getRectangleY(tile.FIRST_RECTANGLE),  tile.getRectangleX(tile.FIRST_RECTANGLE) - 1) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.SECOND_RECTANGLE), tile.getRectangleX(tile.SECOND_RECTANGLE) - 1) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.THIRD_RECTANGLE), tile.getRectangleX(tile.THIRD_RECTANGLE) - 1) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.FOURTH_RECTANGLE), tile.getRectangleX(tile.FOURTH_RECTANGLE) - 1) != 1
        );
    }
    public boolean isShapeMovableRight (Tile tile){
        return  (
                Playground.getPlaygroundDimension( tile.getRectangleY(tile.FIRST_RECTANGLE), tile.getRectangleX(tile.FIRST_RECTANGLE) + 1) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.SECOND_RECTANGLE), tile.getRectangleX(tile.SECOND_RECTANGLE) + 1) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.THIRD_RECTANGLE), tile.getRectangleX(tile.THIRD_RECTANGLE) + 1) != 1
                && Playground.getPlaygroundDimension( tile.getRectangleY(tile.FOURTH_RECTANGLE), tile.getRectangleX(tile.FOURTH_RECTANGLE) + 1) != 1
        );
    }
}
