package model.movement;

import model.game.Game;
import model.game.Playground;
import model.shape.Tile;

import java.util.ArrayList;

class LineDestroyer {
    private Game game = Playground.getCurrentGame();

    void eraseLine(ArrayList<Tile> unmovableShapes){
        ArrayList<Integer> l;
        l = isLineComplete();

        if(l.size()  != 0) {

            for (int line : l) {
                    for (Tile t : unmovableShapes) {
                        for(int j = 0; j < t.getNB_OF_RECTANGLES(); j++) {
                            if (line == t.getRectangleY(j) && Playground.getPlaygroundDimension(line,j) == 1) {
                                t.setRectangleAt(t.getRectangleX(j), line, Playground.BACKGROUND_COLOR);
                            }
                        }
                }
            }
            game.setScore(game.getScore() + Game.POINTS_PER_LINES * l.size());
            moveDownPLAYGROUND_DIMENSION(l);
            moveDownAllBlocks(unmovableShapes, l);
            checkColoration(unmovableShapes);
    }
}

    private void checkColoration(ArrayList<Tile> unmovableShapes){
        for (Tile t : unmovableShapes) {
            for (int j = 0; j < t.getNB_OF_RECTANGLES(); j++) {
                if(Playground.getPlaygroundDimension(t.getRectangleY(j) ,t.getRectangleX(j)) == 1)
                    t.setRectangleColor(t.getShapeColor(),j);
                else
                    t.setRectangleColor(Playground.BACKGROUND_COLOR, j);
            }
        }
    }

    private void moveDownAllBlocks(ArrayList<Tile> unmovableShapes,ArrayList<Integer> lines){
        for (Tile t : unmovableShapes) {
            for (int j = 0; j < t.getNB_OF_RECTANGLES(); j++) {
                if(t.getRectangleColor(j) == Playground.BACKGROUND_COLOR && Playground.getPlaygroundDimension(t.getRectangleY(j),t.getRectangleX(j)) == 1)
                    t.setRectangleColor(t.getShapeColor(), j);

            }
        }
        for (Tile t : unmovableShapes) {
            for(int j = 0; j < t.getNB_OF_RECTANGLES(); j++) {
                if(t.getRectangleY(j) != Playground.PLAYGROUND_ROWS-1 && !lines.contains(t.getRectangleY(j))
                && Playground.getPlaygroundDimension(t.getRectangleY(j), t.getRectangleX(j)) == 1)
                    t.setRectangleColor(t.getShapeColor(), j);
                    t.setRectangleY(t.getRectangleY(j)+1, j);
            }
        }

    }

    private void moveDownPLAYGROUND_DIMENSION(ArrayList<Integer> lines){
        for (int i =0 ; i < lines.size(); i++){
            Playground.setPlaygroundDimension(i, 0);
        }
        for (int i = (lines.get(lines.size()-1)-1); i > 0; i--){
            for (int j = 0; j < Playground.PLAYGROUND_COLS; j++){
                Playground.setPlaygroundDimension(i+lines.size(), j, Playground.getPlaygroundDimension(i,j));
            }
        }
    }

    private ArrayList<Integer> isLineComplete() {
        boolean lineFinished = true;
        ArrayList<Integer> lines = new ArrayList<>();
        for (int i = Playground.PLAYGROUND_ROWS - 1; i > 0; i--) {
            for (int j = 0; j < Playground.PLAYGROUND_COLS; j++) {
                if (Playground.getPlaygroundDimension(i,j) == 0) {
                    lineFinished = false;
                    break;
                }
            }
            if (lineFinished)
                lines.add(i);
            lineFinished = true;
        }
        return lines;
    }
}
