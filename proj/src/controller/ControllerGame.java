package controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.game.Game;
import model.game.Playground;
import model.movement.ShapeMover;
import model.shape.Block;
import model.shape.BlockGenerator;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


public class ControllerGame {

    private static final int PANE_HEIGHT = Playground.TILE_SIZE * 23;
    private static final int PANE_WIDTH = Playground.TILE_SIZE * 13;
    private ShapeMover mover = new ShapeMover();
    private Scene scene;
    private boolean gameEnded = false;
    private final int TIME_TO_GO_DOWN = 750;

    @FXML
    private Label labelScore;
    @FXML
    private Label labelScoreText;
    @FXML
    private BorderPane nextBlockBP;
    @FXML
    private Button btQuit;
    @FXML
    private Button btRet;
    @FXML
    private BorderPane bp;


    private Game g = Playground.getCurrentGame();
    private BlockGenerator generator = g.getBlockGenerator();

    private Block currentBlock;
    {
        try {
            currentBlock = generator.generateBlock();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Block nextBlock;
    {
        try {
            nextBlock = generator.generateBlock();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void initialize(){
        g.setIsGamePlayable(true);

        bp.setMinHeight(PANE_HEIGHT);
        bp.setMinWidth(PANE_WIDTH);

        bp.getChildren().add(g.getGamePlayground().getPlayground());

        for (int i = 0; i < currentBlock.getTile().getNB_OF_RECTANGLES(); i++)
            bp.getChildren().add(currentBlock.getTile().getRectangle(i));

        for (int i = 0; i < nextBlock.getTile().getNB_OF_RECTANGLES(); i++)
            nextBlockBP.getChildren().add(nextBlock.getTile().getRectangle(i));

        labelScoreText.setText("Score :");
        labelScore.textProperty().bind(g.scoreProperty().asString());



        final Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if(!g.getIsGamePlayable()){
                    Platform.runLater(() -> {
                        endGame();
                        Stage stage = (Stage) btQuit.getScene().getWindow();
                        stage.close();
                        }
                    );

                }
                if(gameEnded){
                    Platform.runLater(() -> {
                        Stage stage = (Stage) btQuit.getScene().getWindow();
                        stage.close();
                    });
                }
                if(!mover.isShapeMovable(getCurrentBlock().getTile()) && !gameEnded)
                {
                    Platform.runLater(() -> setNewBlock());
                }
                mover.moveBottom(currentBlock.getTile());
                timer.purge();

            }
        };

        timer.schedule(task, 0, TIME_TO_GO_DOWN);

    }

    void getMovement(Scene s){

        mover.setTile(getCurrentBlock().getTile());
        setScene(s);
        s.setOnKeyPressed(this::controlMovement);

    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Scene getScene() {
        return scene;
    }

    private Block getCurrentBlock() {
        return currentBlock;
    }

    private void endGame(){
        if(!gameEnded){
            try {
                Stage stage = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/endScreen.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                scene.getStylesheets().addAll(this.getClass().getResource("/css/background.css").toExternalForm());
                stage.setTitle("End");
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            gameEnded = true;
        }
    }

    private void controlMovement(KeyEvent key){

        if(!g.getIsGamePlayable()){
            endGame();
            return;
        }
        if(!mover.isShapeMovable(getCurrentBlock().getTile()) && !gameEnded)
            setNewBlock();

        if(g.getIsGamePlayable()) {
            switch (key.getCode()) {
                case S:
                    mover.moveBottom(getCurrentBlock().getTile());
                    break;
                case D:
                    for (int i = 0; i < getCurrentBlock().getTile().getNB_OF_RECTANGLES(); i++) {
                        if (getCurrentBlock().getTile().getRectangleX(i) >= Playground.PLAYGROUND_COLS - 1)
                            return;
                    }
                    if (!mover.isShapeMovableRight(getCurrentBlock().getTile())) {
                        break;
                    }
                    mover.moveRight(getCurrentBlock().getTile());
                    break;
                case Q:
                    for (int i = 0; i < getCurrentBlock().getTile().getNB_OF_RECTANGLES(); i++) {
                        if (getCurrentBlock().getTile().getRectangleX(i) <= 0)
                            return;
                    }
                    if (!mover.isShapeMovableLeft(getCurrentBlock().getTile())) {
                        break;
                    }
                    mover.moveLeft(getCurrentBlock().getTile());
                    break;
                case A:
                    getCurrentBlock().antiClockwiseRotation();
                    break;
                case E:
                    getCurrentBlock().clockwiseRotation();
                    break;
            }
        }
    }

    private void setNewBlock(){
        mover.lockPiece(getCurrentBlock().getTile());
        setCurrentBlock(nextBlock);

        try {
            nextBlock = generator.generateBlock();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mover.setLockedBlock(false);
        getMovement(getScene());
        for (int i = 0; i < currentBlock.getTile().getNB_OF_RECTANGLES(); i++)
            bp.getChildren().add(getCurrentBlock().getTile().getRectangle(i));
        for (int i = 0; i < nextBlock.getTile().getNB_OF_RECTANGLES(); i++)
            nextBlockBP.getChildren().add(nextBlock.getTile().getRectangle(i));
    }
    private void setCurrentBlock(Block currentBlock) {
        this.currentBlock = currentBlock;
    }

    @FXML
    public void quitter() {
        Stage stage = (Stage) btQuit.getScene().getWindow();
        gameEnded = true;
        stage.close();

    }
    @FXML
    public void retour() throws IOException {
        Stage stage = new Stage();
        Stage stage2 = (Stage) btRet.getScene().getWindow();
        stage2.close();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/menu.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(this.getClass().getResource("/css/background.css").toExternalForm());
        stage.setTitle("Menu");
        stage.setScene(scene);
        stage.show();
    }
}
