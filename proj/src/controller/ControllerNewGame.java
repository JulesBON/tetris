package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import model.game.Game;
import model.game.Playground;

import java.io.IOException;

public class ControllerNewGame {
    @FXML
    Button btnOui;

    @FXML
    Button btnNon;


    public void oui(ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/menu.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(this.getClass().getResource("/css/background.css").toExternalForm());
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
    }

    public void non(ActionEvent actionEvent) {

    }
}
