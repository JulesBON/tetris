package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;

public class ControllerMenu {
    @FXML
    private Button btQuit;
    @FXML
    private Button btGame;

    @FXML
    private void initialize(){
        Font f=new Font("Arial",  28);
        btGame.setFont(f);
        btQuit.setFont(f);
    }

    @FXML
    public void jouer() throws IOException {
        Stage stage = new Stage();
        Stage stage2 = (Stage) btGame.getScene().getWindow();
        stage2.close();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/tetris.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(this.getClass().getResource("/css/background.css").toExternalForm());
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
        ControllerGame controller = loader.getController();
        controller.getMovement(scene);
    }

    @FXML
    public void quitter() {
        Stage stage = (Stage) btQuit.getScene().getWindow();
        stage.close();

    }
}
