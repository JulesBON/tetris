package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.game.Game;
import model.game.Playground;
import model.player.Player;
import save.playerInteractions;

import java.io.IOException;

public class ControllerEndGame {
    @FXML
    private Label labelScore;
    @FXML
    private Button btSubmit;
    @FXML
    private TextField Name;
    @FXML
    private FlowPane displayBestScores;
    @FXML
    private FlowPane fpName;

    private Game g = Playground.getCurrentGame();
    private playerInteractions saver = new playerInteractions();


    @FXML
    private void initialize(){

        saver.load();

        if(g.getScore() <= saver.getBestPlayersArray().get(saver.getBestPlayersArray().size()-1).getScore()){
            fpName.setVisible(false);
            btSubmit.setVisible(false);
        }

        Font f=new Font("Arial",  22);
        btSubmit.setFont(f);
        labelScore.setText(String.valueOf(g.getScore()));

        loadScoreboard();
    }

    public void submit(ActionEvent actionEvent) throws IOException {
        Player p = new Player(g.getScore(), Name.getText());
        saver.save(p);
        Stage stage = (Stage) btSubmit.getScene().getWindow();
        Stage stage1 = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/NouvellePartie.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(this.getClass().getResource("/css/background.css").toExternalForm());
        stage1.setTitle("New Game");
        stage1.setScene(scene);
        stage1.show();
        stage.close();
    }

    private void loadScoreboard(){
        if(displayBestScores.getChildren().size() != 0){
            displayBestScores.getChildren().remove(0, saver.getBestPlayersArray().size());
        }

        for(Player player : saver.getBestPlayersArray()){
            Text t = new Text(player.getName() + "  \t\t\t" + player.getScore());
            t.setStyle("-fx-font: 20 arial;");
            displayBestScores.getChildren().add(t);
        }
    }


}
